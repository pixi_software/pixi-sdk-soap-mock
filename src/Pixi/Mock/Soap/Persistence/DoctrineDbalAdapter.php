<?php
namespace Pixi\Mock\Soap\Persistence;

class DoctrineDbalAdapter extends PersistenceAbstract implements PersistenceInterface
{

    public $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function setupTables()
    {

        foreach($this->tables as $table) {
            $this->conn->query($table);
        }

    }

    public function createTestCase()
    {
        $this->conn->query("INSERT INTO `api_mock` (`test_case`, `api_call`, `expected_request`, `actual_request`, `result`, `used`) VALUES
        ('OrderUpdate', 'pixiGetShops', '{\"Argv\":\"Foo\"}', NULL, '{\"ShopID\":\"Works!\"}', 0);");
    }

    public function doRequest($action, $parameters)
    {
        $error = array();
        $result = array();

        $query = $this->conn->fetchAssoc("SELECT * FROM $this->apiMockTable WHERE api_call = ? AND used = 0", [$action]);

        if($query == false) {
            $error[] = array('Message' => 'No Mock found!', 'Number' => 0);
        } else {
            $result = json_decode($query['result'], true);
        }

        // Update the actual request for later comparision in the automated tests
        $this->conn->update($this->apiMockTable, ['actual_request' => json_encode($parameters), 'used' => 1], ['id' => $query['id']]);

        return ['resultSet' => $result, 'error' => $error];

    }

}
