<?php

namespace Pixi\Mock\Soap\Persistence;

class ActiveRecordAdapter extends PersistenceAbstract implements PersistenceInterface
{

    public $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function setupTables()
    {

        foreach($this->tables as $table) {
            $this->conn->query($table);
        }

    }

    public function doRequest($action, $parameters)
    {
        $error = array();
        $result = array();

        $query = $this->conn->get_where($this->apiMockTable, ['api_call' => $action, 'used' => 0]);

        if($query == false) {
            $error[] = array('Message' => 'DB error', 'Number' => 0);
        }

        $rs = $query->result_array();

        if(count($rs) == 0) {
            $error[] = array('Message' => 'DB error', 'Number' => 0);
        } else {
            $result = json_decode($rs[0]['result'], true);
        }

        // Update the actual request for later comparision in the automated tests
        $this->conn->update($this->apiMockTable, ['actual_request' => json_encode($parameters), 'used' => 1], ['id' => $rs[0]['id']]);

        return ['resultSet' => $result, 'error' => $error];

    }

}
