<?php
namespace Pixi\Mock\Soap\Persistence;

abstract class PersistenceAbstract
{

    public $apiMockTable = 'api_mock';

    public $tables = array(

        'api_mock' => "CREATE TABLE IF NOT EXISTS `api_mock` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `test_case` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
          `api_call` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
          `expected_request` text COLLATE utf8_unicode_ci,
          `actual_request` text COLLATE utf8_unicode_ci,
          `result` text COLLATE utf8_unicode_ci,
          `used` tinyint(1) unsigned DEFAULT 0 NOT NULL,
           PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"

    );

}
