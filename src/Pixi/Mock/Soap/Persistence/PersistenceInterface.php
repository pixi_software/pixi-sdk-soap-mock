<?php

namespace Pixi\Mock\Soap\Persistence;

interface PersistenceInterface
{

    public function setupTables();

    public function doRequest($action, $parameters);

}
