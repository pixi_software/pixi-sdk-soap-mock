<?php

namespace Pixi\Mock\Soap\Transport;

use Pixi\API\Soap\Transport\TransportInterface;
use Pixi\Mock\Soap\Persistence\PersistenceInterface;


class MockTransport implements TransportInterface
{

    public $options;

    public $persistance;

    private $request;

    public function __construct(PersistenceInterface $persistance = null)
    {
        if($persistance !== null) {
            $this->setPersistance($persistance);
        }
    }

    public function setPersistance(PersistenceInterface $persistance)
    {
        $this->persistance = $persistance;
        return $this;
    }

    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    public function __doRequest($request, $location = NULL, $action = NULL, $version = NULL)
    {
        echo '<pre>';
        $this->setRequest($request);

        $action = $this->getAction();
        $parameters = $this->getParameters();

        $result = $this->persistance->doRequest($action, $parameters);

        return $result;
    }

    private function getAction()
    {
        $actionKeys = array_keys($this->request['envBody']);

        $actionKey = str_replace('ns1', '', $actionKeys[0]);
        return $actionKey;
    }

    private function getParameters()
    {

        $parameters = array();
        foreach(reset($this->request['envBody']) as $parameter => $value) {

            $param = str_replace('ns1', '', $parameter);
            $parameters[$param] = $value;

        }

        return $parameters;

    }

    private function setRequest($request)
    {
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $request);
        $xml = simplexml_load_string($xml);
        $this->request = json_decode(json_encode($xml), true);
    }

}
