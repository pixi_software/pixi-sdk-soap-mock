<?php

// Including the autoloader and an Subscriber
require __DIR__.'/../vendor/autoload.php';

$username = 'pixiAPP';
$password = 'fHNzq44NA6kaDm_APP';
$endpoint = 'https://api.pixi.eu/soap/pixiAPP/';

$options = new Pixi\API\Soap\Options($username, $password, $endpoint);

$options->allowSelfSigned();
$soapClient = new Pixi\API\Soap\Client(null, $options->getOptions());

$config = new Doctrine\DBAL\Configuration();
$conn = \Doctrine\DBAL\DriverManager::getConnection([
    'dbname'    => 'api_test',
    'user'      => 'root',
    'host'      => 'localhost',
    'driver'    => 'pdo_mysql'
]);

$persistence = new Pixi\Mock\Soap\Persistence\DoctrineDbalAdapter($conn);
$persistence->setupTables();

// Just for easier testing
$persistence->createTestCase();

$transport = new Pixi\Mock\Soap\Transport\MockTransport($persistence);
$soapClient->setTransportObject($transport);

$soapClient->setResultObject('\Pixi\API\Soap\Result\ArrayResult');

$rs = $soapClient->pixiGetShops(['ShopID' => 'FLO', 'Param2' => 'arg2'])->getResultSet();

var_dump($rs);

